var controller = require('../application/controller');

module.exports = function (app) {

    var c = controller(app);

    c.drawIndex = function (req, res) {
        if (req.isAuthenticated())
            res.redirect('/home');
        else
            res.render('index', {test: app.controllers});
    };
    app.get('/', c.drawIndex);

    return c;

}