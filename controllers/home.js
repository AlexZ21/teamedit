var controller = require('../application/controller');

module.exports = function (app) {

    var c = controller(app);

    c.drawHome = function (req, res) {
        res.redirect('/u/' + req.user.local.email);
    };
    app.get('/home', c.drawHome);
    app.get('/u', c.drawHome);

    c.drawUser = function (req, res) {
        if (req.isAuthenticated()) {
            var path = '';
            if (req.params[0])
                path = req.params[0];

            res.render('home', {userName: req.user.local.name,
                targetUserName: req.params.userName,
                targetPath: path});
        } else
            res.redirect('/');
    }
    app.get('/u/:userName', c.drawUser);
    app.get('/u/:userName/*', c.drawUser);

    return c;

}