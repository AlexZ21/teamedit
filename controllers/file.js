var controller = require('../application/controller');
var files = require('../application/files');
var User = require('../application/models/user');
var File = require('../application/models/file');
var Room = require('../application/room');
var util = require('../application/util');
var git = require('../application/git');
var multer = require('multer');

module.exports = function (app) {

    var c = controller(app);

    c.getFile = function (req, res) {

        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var filePath = '/';

                    if (typeof (req.params[0]) !== 'undefined') {
                        filePath += req.params[0];
                    }

                    var path = app.config.users_dir + user._id + filePath;
                    path = path.replace(/\/\/+/, "/");

                    if (path.substr(path.length - 1) == '/')
                        path = path.substring(0, path.length - 1);

                    var roomIdPath = user.local.name + filePath;
                    roomIdPath = roomIdPath.replace(/\/\/+/, "/");


                    File.findOne({path: path}, function (err, file) {
                        
                        

                        if (file && (req.user.local.name == file.owner || file.users.indexOf(req.user.local.name) >= 0) && !err) {

                            files.fileType(app.config.users_dir + user._id + filePath, function (type) {

                                if (type === 'file') {
                                    
                                    console.log('New room: ',user.local.name + filePath);
                                   

                                    if (!(c.app.rooms[roomIdPath])) {

                                        if (c.app.editors[files.getFileMime(filePath)]) {
                                            c.app.rooms[roomIdPath] =
                                                    new Room(c.app, roomIdPath,
                                                            app.config.users_dir + user._id + filePath);

                                            c.app.roomsLinks[util.makeRoomId()] = roomIdPath;

                                            c.app.rooms[roomIdPath].
                                                    setEditor(new c.app.editors[files.getFileMime(filePath)]());

                                            var editor = c.app.rooms[roomIdPath].getEditor();

                                            c.app.rooms[roomIdPath].open();

                                            res.render(editor.getClient().ejs, {targetPort: c.app.rooms[roomIdPath].port}, function (err, html) {

                                                var response = {
                                                    data: {
                                                        html: html,
                                                        dep: editor.getClient().files,
                                                        path: filePath,
                                                        port: c.app.rooms[roomIdPath].port
                                                    }

                                                };

                                                res.send(JSON.stringify(response));
                                            });

                                        } else {
                                            var response = {
                                                message: {type: 'danger', msg: "Can't open this file!"}
                                            };

                                            res.send(JSON.stringify(response));
                                        }

                                    } else {
                                        
                                        console.log('Exist: ', user.local.name + filePath);
                                        
                                        var editor = c.app.rooms[roomIdPath].getEditor();

                                        res.render(editor.getClient().ejs, {targetPort: c.app.rooms[roomIdPath].port}, function (err, html) {

                                            var response = {
                                                data: {
                                                    html: html,
                                                    dep: editor.getClient().files,
                                                    path: filePath
                                                }

                                            };

                                            res.send(JSON.stringify(response));
                                        });
                                    }



                                } else {

                                    filePath += '/';
                                    files.getFileList(app.config.users_dir + user._id, filePath, function (f) {

                                        res.render('filelist', {}, function (err, html) {

                                            var response = {
                                                data: {
                                                    html: html,
                                                    dep: [{type: 'js', path: '/public/js/filelist.js'}],
                                                    path: filePath
                                                }

                                            };

                                            res.send(JSON.stringify(response));
                                        });
                                    });



                                }

                            });


                        } else {
                            var response = {};
                            response.message = {type: 'danger', msg: "Can't get file"};
                            res.send(JSON.stringify(response));
                        }

                    });

                }

            });

        } else
            res.redirect('/');
    };
    app.get('/file/:userName/*', c.getFile);
    app.get('/file/:userName', c.getFile);


    c.createFile = function (req, res) {
        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var filePath = '/';

                    if (typeof (req.params[0]) !== 'undefined') {
                        filePath += req.params[0];
                    }

                    var path = app.config.users_dir + user._id + filePath;
                    path = path.replace(/\/\/+/, "/");

                    files.createFile(user, path, function (err) {

                        var response = {};

                        if (err) {
                            response.message = {type: 'danger', msg: "Can't create file"};
                        } else {
                            response.message = {type: 'success', msg: "File created"};
                        }

                        res.send(JSON.stringify(response));

                    });


                }
            });

        } else
            res.redirect('/');
    };
    app.post('/file/:userName/*', c.createFile);

    c.getDir = function (req, res) {

        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var filePath = '/';

                    if (typeof (req.params[0]) !== 'undefined') {
                        filePath += req.params[0];
                    }

                    var path = app.config.users_dir + user._id + filePath;
                    path = path.replace(/\/\/+/, "/");

                    if (path.substr(path.length - 1) == '/')
                        path = path.substring(0, path.length - 1);

                    File.findOne({path: path}, function (err, file) {

                        console.log(path, file);

                        if (file && (req.user.local.name == file.owner || file.users.indexOf(req.user.local.name) >= 0) && !err) {

                            files.getFileList(app.config.users_dir + user._id, filePath, function (f) {

                                var response = {
                                    data: {
                                        fileList: f,
                                        path: filePath,
                                        targetUser: user.local.name
                                    }

                                };

                                if (!f) {
                                    response.error = "It's file";
                                }

                                res.send(JSON.stringify(response));
                            });
                        } else {
                            var response = {};
                            response.message = {type: 'danger', msg: "Can't get file"};
                            res.send(JSON.stringify(response));
                        }
                    });
                }
            });

        } else
            res.redirect('/');
    };
    app.get('/dir/:userName/*', c.getDir);
    app.get('/dir/:userName', c.getDir);

    c.createDir = function (req, res) {
        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var filePath = '/';

                    if (typeof (req.params[0]) !== 'undefined') {
                        filePath += req.params[0];
                    }

                    var path = app.config.users_dir + user._id + filePath;
                    path = path.replace(/\/\/+/, "/");


                    files.createDir(user, path, function (err) {

                        var response = {};

                        if (err) {
                            response.message = {type: 'danger', msg: "Can't create directory"};
                        } else {
                            response.message = {type: 'success', msg: "Directory created"};
                        }

                        res.send(JSON.stringify(response));

                    });


                }
            });

        } else
            res.redirect('/');
    };
    app.post('/dir/:userName/*', c.createDir);

    c.removeFile = function (req, res) {
        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var filePath = '/';

                    if (typeof (req.params[0]) !== 'undefined') {
                        filePath += req.params[0];
                    }

                    var path = app.config.users_dir + user._id + filePath;
                    path = path.replace(/\/\/+/, "/");

                    File.findOne({path: path}, function (err, file) {

                        if (file && (req.user.local.name == file.owner || file.users.indexOf(req.user.local.name) >= 0) && !err) {


                            files.fileType(path, function (type) {

                                if (type === 'file') {

                                    files.removeFile(user, path, function (err) {
                                        var response = {};

                                        if (err) {
                                            response.message = {type: 'danger', msg: "Can't remove file"};
                                        } else {
                                            response.message = {type: 'success', msg: "File removed"};
                                        }

                                        res.send(JSON.stringify(response));
                                    });

                                } else {

                                    files.removeDir(user, path, function (err) {
                                        var response = {};

                                        if (err) {
                                            response.message = {type: 'danger', msg: "Can't remove directory"};
                                        } else {
                                            response.message = {type: 'success', msg: "Directory removed"};
                                        }

                                        res.send(JSON.stringify(response));
                                    });

                                }

                            });

                        } else {
                            var response = {};
                            response.message = {type: 'danger', msg: "Can't get file"};
                            res.send(JSON.stringify(response));
                        }
                    });


                }
            });

        } else
            res.redirect('/');
    };
    app.delete('/file/:userName/*', c.removeFile);

    c.uploadFile = function (req, res, next) {
        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var filePath = '/';
                    if (typeof (req.params[0]) !== 'undefined') {
                        filePath += req.params[0];
                    }
                    var path = app.config.users_dir + user._id + filePath;
                    path = path.replace(/\/\/+/, "/");

                    multer({
                        dest: path,
                        rename: function (fieldname, filename, req, res) {
                            return filename;
                        },
                        onFileUploadComplete: function (file, req, res) {

                            console.log('"' + path + file.originalname + '"');

                            git(app.config.users_dir + user._id).exec('add', ['"' + path + file.originalname + '"'], function (gitErr, gitMsg) {
                                console.log(gitErr, gitMsg);
                                git(app.config.users_dir + user._id).exec('commit', ['--author="' + user.local.name + ' <' + user.local.email + '>"', '-m', '"File created"'],
                                        function (gitErr, gitMsg) {
                                            console.log(gitErr, gitMsg);
                                        });
                            });
                        }
                    })(req, res, next);

                    var response = {};
                    response.message = {type: 'success', msg: "Files uploaded"};
                    res.send(JSON.stringify(response));


                }

            });

        } else
            res.redirect('/');
    };
    app.post('/upload/:userName', c.uploadFile);
    app.post('/upload/:userName/*', c.uploadFile);

    c.fileHistoryList = function (req, res) {
        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var filePath = '/';
                    if (typeof (req.params[0]) !== 'undefined') {
                        filePath += req.params[0];
                    }
                    var path = app.config.users_dir + user._id + filePath;
                    path = path.replace(/\/\/+/, "/");

                    File.findOne({path: path}, function (err, file) {

                        if (file && (req.user.local.name == file.owner || file.users.indexOf(req.user.local.name) >= 0) && !err) {


                            files.getFileHistoryList(user, path, function (err, data) {

                                var response = {};

                                if (err) {
                                    response.message = {type: 'danger', msg: "Can't remove directory"};
                                } else {
                                    response.message = {type: 'success', msg: "Directory removed"};
                                    response.data = {data: data};
                                }

                                res.send(JSON.stringify(response));

                            });

                        } else {
                            var response = {};
                            response.message = {type: 'danger', msg: "Can't get file"};
                            res.send(JSON.stringify(response));
                        }
                    });

                }

            });

        } else
            res.redirect('/');
    }
    app.get('/history/list/:userName/*', c.fileHistoryList);

    c.fileHistory = function (req, res) {
        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var commit;
                    if (typeof (req.params[0]) !== 'undefined') {
                        commit = req.params[0];
                    }



                    files.getFileHistory(user, commit, function (err, data) {

                        var response = {};

                        if (err) {
                            response.message = {type: 'danger', msg: "Can't get file history"};
                        } else {
                            response.data = {data: data};
                        }

                        res.send(JSON.stringify(response));

                    });

                }

            });

        } else
            res.redirect('/');
    }
    app.get('/history/:userName/*', c.fileHistory);

    c.fileHistoryRevert = function (req, res) {
        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var commit;
                    if (typeof (req.params[0]) !== 'undefined') {
                        commit = req.params[0];
                    }

                    var filePath = '/';

                    if (typeof (req.params[0]) !== 'undefined') {
                        filePath += req.params[0];
                    }

                    var path = app.config.users_dir + user._id + filePath;
                    path = path.replace(/\/\/+/, "/");

                    files.fileHistoryRevert(user, req.params.commit, path, function (err, data) {

                        var response = {};

                        if (err) {
                            response.message = {type: 'danger', msg: "Can't get file history"};
                        } else {
                            response.message = {type: 'success', msg: "Commit reverted"};
                        }

                        res.send(JSON.stringify(response));

                    });

                }

            });

        } else
            res.redirect('/');
    };
    app.post('/history/revert/:commit/:userName/*', c.fileHistoryRevert);

    c.fileInfo = function (req, res) {
        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var filePath = '/';
                    if (typeof (req.params[0]) !== 'undefined') {
                        filePath += req.params[0];
                    }
                    var path = app.config.users_dir + user._id + filePath;
                    path = path.replace(/\/\/+/, "/");

                    File.findOne({path: path}, function (err, file) {

                        var response = {};

                        if (file && (req.user.local.name == file.owner || file.users.indexOf(req.user.local.name) >= 0) && !err) {


                            response.data = {
                                data: {
                                    path: file.path,
                                    users: file.users,
                                    published: file.published,
                                    owner: file.owner
                                }
                            };

                        } else {
                            response.message = {type: 'danger', msg: "Can't get file prop"};
                        }

                        res.send(JSON.stringify(response));

                    });

                }

            });

        } else
            res.redirect('/');
    };
    app.get('/info/:userName/*', c.fileInfo);

    c.updateFile = function (req, res) {
        if (req.isAuthenticated()) {

            User.findOne({'local.email': req.params.userName}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {

                    var filePath = '/';
                    if (typeof (req.params[0]) !== 'undefined') {
                        filePath += req.params[0];
                    }
                    var path = app.config.users_dir + user._id + filePath;
                    path = path.replace(/\/\/+/, "/");


                    File.findOne({path: path}, function (err, file) {

                        if (file && (req.user.local.name == file.owner || file.users.indexOf(req.user.local.name) >= 0) && !err) {

                            var newFile = req.body;

                            var pathArray = file.path.split('/');
                            pathArray[pathArray.length - 1] = newFile.fileName;
                            var newFilePath = pathArray.join('/');

                            var newUsers = [];

                            newFile.users.forEach(function (newUser) {
                                newUsers.push(newUser.text);
                            });

                            files.rename(user, path, newFilePath, function (err) {

                                file.update({path: newFilePath, users: newUsers, published: newFile.published}, function (err) {

                                    var response = {};

                                    if (err) {
                                        response.message = {type: 'danger', msg: "Can't update file"};
                                    } else {
                                        response.message = {type: 'success', msg: "File updated"};
                                    }

                                    res.send(JSON.stringify(response));

                                });

                            });

                        } else {
                            var response = {};
                            response.message = {type: 'danger', msg: "Can't get file"};
                            res.send(JSON.stringify(response));
                        }
                    });

                }

            });

        } else
            res.redirect('/');
    };
    app.post('/update/:userName/*', c.updateFile);

    return c;

}