var controller = require('../application/controller');

module.exports = function (app) {

    var c = controller(app);

    c.redirectToFile = function (req, res) {
        if (req.isAuthenticated()) {
            
            res.redirect('/u/'+c.app.roomsLinks[req.params.roomId]);
            
        } else
            res.render('index', {test: app.controllers});
    };
    app.get('/r/:roomId', c.redirectToFile);

    return c;

}