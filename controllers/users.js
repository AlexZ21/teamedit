var controller = require('../application/controller');
var passport = require('passport');
var User = require('../application/models/user');

module.exports = function (app) {

    var c = controller(app);

    c.drawLogin = function (req, res) {
        console.log(req.user);
        res.render('login', {});
    };
    app.get('/login', c.drawLogin);
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: 'home',
        failureRedirect: '/login',
        failureFlash: true
    }));

    c.drawSignup = function (req, res) {
        console.log(req.user);
        res.render('signup', {});
    };
    app.get('/signup', c.drawSignup);
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: 'home',
        failureRedirect: '/signup',
        failureFlash: true
    }));

    app.get('/logout', function (req, res) {
        req.session.destroy(function (err) {
            req.logout();
            res.redirect('/');
        });
    });
    
    app.get('/user/list', function (req, res) {
        if (req.isAuthenticated()) {
            
            User.find({ 'local.name': new RegExp(req.query.s, "i") }, function (err, users) {
                
                var userNames = [];
                
                users.forEach(function(user) {
                    userNames.push({text: user.local.name});
                });
                
                res.send(userNames);
                
            });
            
            
        } else 
            res.redirect('/');
    });

    return c;

}