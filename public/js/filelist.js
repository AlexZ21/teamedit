app.controller('filelistController', function ($scope, $http, $contentLoader,
        $sharedProperties, $alertService, $modalService) {

    $scope.init = function () {
        $scope.loadDir($sharedProperties.targetUser + '/' + $sharedProperties.targetPath);
    };

    $scope.loadDir = function (path) {

        if (path.substring(0, 1) == '/')
            path = path.substring(1);

        var fileListRes = $http.get('/dir/' + path);

        fileListRes.success(function (response, status, headers, config) {

            if (!response.message) {

                $scope.fileList = [];

                for (fileName in response.data.fileList) {

                    response.data.fileList[fileName].path = response.data.targetUser + response.data.fileList[fileName].path;

                    $scope.fileList.push(response.data.fileList[fileName]);
                }

                $scope.pathList = [];
                $scope.pathList.push({dirName: $sharedProperties.targetUser, path: $sharedProperties.targetUser});
                $scope.dirPath = response.data.path;

                var pathString = '/';
                var pathList = response.data.path.split('/');
                pathList.forEach(function (path) {
                    if (path) {
                        pathString += path + '/';
                        $scope.pathList.push({dirName: path, path: $sharedProperties.targetUser + pathString});
                    }
                });
            } else {
                $alertService.push(response.message);
            }

            //$scope.$apply();

        });

    }

    $scope.loadFile = function (path) {
        console.log("/file/" + path);
        $contentLoader.getUrl("/file/" + path);
    }

    $scope.modal = function (template, controller) {
        $modalService.open(template, controller, {
            fsProp: function () {
                return {dirPath: $scope.dirPath};
            }});
    }

    $scope.filePropety = function () {
        $modalService.open('prop-file-modal', 'propFileModalController', {
            fsProp: function () {
                return {dirPath: $scope.dirPath, targetFile: event.target.data};
            }}, 'lg');
    }

    $scope.removeFile = function (event) {
        $modalService.open('remove-file-modal', 'removeFileModalController', {
            fsProp: function () {
                return {dirPath: $scope.dirPath, targetFile: event.target.data};
            }});
    }

    $scope.contextMenu = [
        {title: 'Параметры', event: 'filePropety'},
        null,
        {title: 'Удаить', event: 'removeFile'}
    ];

});

app.controller('createFileModalController', function ($scope, $modalInstance,
        $sharedProperties, $http, $alertService, fsProp) {
    $scope.ok = function () {

        if (fsProp.dirPath.substring(0, 1) == '/')
            fsProp.dirPath = fsProp.dirPath.substring(1);

        var createFileRes = $http.post('/file/' + $sharedProperties.targetUser + '/' +
                fsProp.dirPath + '/' + $scope.fileName);

        createFileRes.success(function (response, status, headers, config) {
            $alertService.push(response.message);
            angular.element(document.querySelector('[ng-controller=filelistController]')).
                    scope().loadDir($sharedProperties.targetUser +
                    fsProp.dirPath);
        });

        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

app.controller('createDirModalController', function ($scope, $modalInstance,
        $sharedProperties, $http, $alertService, fsProp) {
    $scope.ok = function () {

        var createFileRes = $http.post('/dir/' + $sharedProperties.targetUser + '/' +
                fsProp.dirPath + '/' + $scope.dirName);

        createFileRes.success(function (response, status, headers, config) {
            $alertService.push(response.message);
            angular.element(document.querySelector('[ng-controller=filelistController]')).
                    scope().loadDir($sharedProperties.targetUser +
                    fsProp.dirPath);
        });

        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

app.controller('removeFileModalController', function ($scope, $modalInstance,
        $sharedProperties, $http, $alertService, fsProp) {
    $scope.ok = function () {

        var createFileRes = $http.delete('/file/' + fsProp.targetFile);

        createFileRes.success(function (response, status, headers, config) {
            $alertService.push(response.message);
            angular.element(document.querySelector('[ng-controller=filelistController]')).
                    scope().loadDir($sharedProperties.targetUser +
                    fsProp.dirPath);
        });

        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

app.controller('propFileModalController', function ($scope, $modalInstance,
        $sharedProperties, $http, $alertService, fsProp) {

    $scope.file = {
        users: [],
        fileName: '',
        owner: '',
        published: 0,
        type: '',
        path: ''
    };

    $scope.loadUsers = function (query) {
        return $http.get('/user/list?s=' + query);
    };


    var fileInfoRes = $http.get('/info/' + fsProp.targetFile);

    fileInfoRes.success(function (response, status, headers, config) {
        
        if (response.message && response.message.type === 'danger') {
            $alertService.push(response.message);
            $modalInstance.close();
        } else
            $scope.file = response.data.data;
        
            var pathArray = $scope.file.path.split('/');
            $scope.file.fileName = pathArray[pathArray.length - 1];
        
            console.log($scope.file);
            
    });


    var fileHistoryListRes = $http.get('/history/list/' + fsProp.targetFile);

    fileHistoryListRes.success(function (response, status, headers, config) {
        $scope.commits = response.data.data;
    });

    $scope.getFileHistory = function (commit) {
        var fileHistoryRes = $http.get('/history/' + $sharedProperties.targetUser + '/' + commit);

        fileHistoryRes.success(function (response, status, headers, config) {
            $scope.history = response.data.data;
        });
    };

    $scope.revert = function (commit) {

        var fileHistoryRevertRes = $http.post('/history/revert/' + commit + '/' +
                fsProp.targetFile);

        fileHistoryRevertRes.success(function (response, status, headers, config) {
            $alertService.push(response.message);
            $modalInstance.close();
        });



    };

    $scope.ok = function () {

        var updateFileRes = $http.post('/update/' + fsProp.targetFile, $scope.file);

        updateFileRes.success(function (response, status, headers, config) {
            $alertService.push(response.message);
        });

        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

app.controller('uploadModalController', function ($scope, $modalInstance,
        $sharedProperties, $http, $alertService, FileUploader, fsProp) {

    $scope.uploader = new FileUploader({
        url: '/upload/' + $sharedProperties.targetUser + fsProp.dirPath
    });

    $scope.ok = function () {

//        var createFileRes = $http.delete('/file/' + fsProp.targetFile);
//
//        createFileRes.success(function (response, status, headers, config) {
//            $alertService.push(response.message);
        angular.element(document.querySelector('[ng-controller=filelistController]')).
                scope().loadDir($sharedProperties.targetUser +
                fsProp.dirPath);
//        });

        $modalInstance.close();
    };

    $scope.cancel = function () {
        angular.element(document.querySelector('[ng-controller=filelistController]')).
                scope().loadDir($sharedProperties.targetUser +
                fsProp.dirPath);
        $modalInstance.dismiss('cancel');
    };
});