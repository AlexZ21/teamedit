var app = angular.module('teamedit', ['ui.bootstrap', 'angularFileUpload', 'ngTagsInput']);

app.config(function ($controllerProvider, $provide, $compileProvider) {

    app._controller = app.controller;
    app._service = app.service;
    app._factory = app.factory;
    app._value = app.value;
    app._directive = app.directive;


    app.controller = function (name, constructor) {

        $controllerProvider.register(name, constructor);
        return(this);

    };


    app.service = function (name, constructor) {

        $provide.service(name, constructor);
        return(this);

    };


    app.factory = function (name, factory) {

        $provide.factory(name, factory);
        return(this);

    };


    app.value = function (name, value) {

        $provide.value(name, value);
        return(this);

    };


    app.directive = function (name, factory) {

        $compileProvider.directive(name, factory);
        return(this);

    };

}
);

app.service('$sharedProperties', function () {
    return {
    };
});

app.service('$contentLoaderResponse', function () {
    return {
        response: {}
    };
});

app.factory('$contentLoader', function ($http, $compile, $rootScope, $contentLoaderResponse, $alertService) {
    return {
        getUrl: function (url) {
            var self = this;

            var content = $http.get(url);

            content.success(function (response, status, headers, config) {

                $contentLoaderResponse.response = response;

                if (!response.message) {

                    if (document.querySelector('.content-container div')) {

                        angular.element(document.querySelector('.content-container div')).scope().$destroy();
                        document.querySelectorAll('.content-dep').remove();
                        document.querySelector('.content-container div').remove();
                    }


                    self.getDep(response.data.dep, 'content-dep', function () {
                        document.querySelector('.content-container').innerHTML = response.data.html;
                        var contentDom = document.querySelector('.content-container div');
                        $compile(contentDom)($rootScope);
                    });
                } else {
                    $alertService.push(response.message);
                }

            });
        },
        getDep: function (filesArray, group, callback) {
            var fileCount = filesArray.length;
            var fileCountCurrent = 0;

            var element = {};
            while (element = filesArray.shift()) {
                if (element.type === 'js') {
                    var fileref = document.createElement('script');
                    fileref.type = 'text/javascript';
                    fileref.addEventListener("load", checkLoad, false);
                    fileref.src = element.path;
                    if (group) {
                        fileref.className = group;
                    }
                    if (typeof (fileref) !== "undefined")
                        document.getElementsByTagName("head")[0].appendChild(fileref);
                } else if (element.type === 'css') {
                    var fileref = document.createElement("link");
                    fileref.rel = 'stylesheet';
                    fileref.type = 'text/css';
                    fileref.href = element.path;
                    fileref.addEventListener("load", checkLoad, false);
                    if (group) {
                        fileref.className = group;
                    }
                    if (typeof (fileref) !== "undefined")
                        document.getElementsByTagName("head")[0].appendChild(fileref);
                }

            }

            function checkLoad() {
                fileCountCurrent++;
                if (fileCountCurrent == fileCount) {
                    if (callback && typeof (callback) === "function") {
                        callback();
                    }
                }
            }
        }
    };
});

app.service('$alertService', function () {
    return {
        alerts: [],
        callback: null,
        scope: null,
        push: function (alert) {
            this.alerts.push(alert);
            
            if (this.scope) {
                this.scope.$apply;
            }
            
        }
    };
});

app.service('$modalService', function ($modal) {
    return {
        open: function (template, controller, resolve, size) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: template,
                controller: controller,
                resolve: resolve,
                size: size
            });
        }
    };
});

app.directive('ngRightClick', function ($parse) {

    var makeContextMenu = function ($scope, contextMenu, data, event) {

        document.querySelectorAll('.dropdown-menu.open').remove();

        var ulContextMenu = document.createElement('ul');
        ulContextMenu.className = 'dropdown-menu open';
        ulContextMenu.role = 'menu';

        ulContextMenu.style.display = 'block';
        ulContextMenu.style.position = 'absolute';
        ulContextMenu.style.top = event.pageY + 'px';
        ulContextMenu.style.left = event.pageX + 'px';

        $scope[contextMenu].forEach(function (item) {
            var liItem = document.createElement('li');

            if (item) {
                var liItemA = document.createElement('a');
                liItemA.innerHTML = item.title;
                liItemA.data = data;
                liItemA.addEventListener('click', $scope[item.event]);
                liItem.appendChild(liItemA);
            } else {
                liItem.className = 'divider';
            }

            ulContextMenu.appendChild(liItem);

        });

        document.body.appendChild(ulContextMenu);

    };

    return {
        scope: true,
        link: function ($scope, element, attrs) {
            //var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function (event) {
                $scope.$apply(function () {
                    event.preventDefault();
                    makeContextMenu($scope, attrs.ngRightClick, attrs.data, event);
                    //fn($scope, {$event: event});
                });
            });
        }
    };


});

app.controller('homeController', function ($scope, $http, $compile, $contentLoader, $sharedProperties, $alertService) {

    $scope.init = function () {


        $sharedProperties.targetUser = document.querySelector('input[name=target-user]').value;
        $sharedProperties.targetPath = document.querySelector('input[name=target-path]').value;

        $contentLoader.getUrl("/file/" + $sharedProperties.targetUser + '/' + $sharedProperties.targetPath);

        $scope.alerts = $alertService.alerts;
        $alertService.scope = $scope;

        document.addEventListener('click', function (e) {
            document.querySelectorAll('.dropdown-menu.open').remove();
        });
    }

    $scope.closeAlert = function (index) {
        $alertService.alerts.splice(index, 1);
    }

});

function loadDep(filesArray, group, callback) {

    var fileCount = filesArray.length;
    var fileCountCurrent = 0;

    while (element = filesArray.shift()) {
        if (element.type === 'js') {
            var fileref = document.createElement('script');
            fileref.type = 'text/javascript';
            fileref.addEventListener("load", checkLoad, false);
            fileref.src = element.path;
            if (group) {
                fileref.className = group;
            }
            if (typeof (fileref) !== "undefined")
                document.getElementsByTagName("head")[0].appendChild(fileref);
        } else if (element.type === 'css') {
            var fileref = document.createElement("link");
            fileref.rel = 'stylesheet';
            fileref.type = 'text/css';
            fileref.href = element.path;
            fileref.addEventListener("load", checkLoad, false);
            if (group) {
                fileref.className = group;
            }
            if (typeof (fileref) !== "undefined")
                document.getElementsByTagName("head")[0].appendChild(fileref);
        }

    }

    function checkLoad() {
        fileCountCurrent++;
        if (fileCountCurrent == fileCount) {
            if (callback && typeof (callback) === "function") {
                callback();
            }
        }
    }
}


Element.prototype.remove = function () {
    this.parentElement.removeChild(this);
}

NodeList.prototype.remove = HTMLCollection.prototype.remove = function () {
    for (var i = 0, len = this.length; i < len; i++) {
        if (this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}