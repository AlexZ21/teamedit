function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

app.controller('editorController', function ($scope, $contentLoader, $contentLoaderResponse, 
        $sharedProperties, $alertService) {

    $scope.init = function () {

        $scope.pathList = [];
        $scope.pathList.push({dirName: $sharedProperties.targetUser, path: $sharedProperties.targetUser});
        $scope.dirPath = $contentLoaderResponse.response.path;

        var pathString = '/';
        var pathList = $contentLoaderResponse.response.data.path.split('/');
        pathList.forEach(function (path) {
            if (path) {
                pathString += path;
                $scope.pathList.push({dirName: path, path: $sharedProperties.targetUser + pathString});
            }
        });

        $scope.port = document.querySelector('input[name=target-port]').value;

        $scope._quill = new Quill('.editor-container', {
            modules: {

                'toolbar': {
                    container: '.advanced-wrapper .toolbar-container'
                },
                'link-tooltip': true,
                'image-tooltip': true,
                'multi-cursor': true
            },
            styles: false,
            theme: 'snow'
        });

        //$scope.authorship = $scope._quill.getModule('authorship');
        $scope.cursorManager = $scope._quill.getModule('multi-cursor');

        $scope.socket = io('http://teamedit:' + $scope.port);

        $scope.socket.on('connect', function () {

        });

        $scope.socket.on('first-user', function (data) {
            $scope._quill.setHTML(data);
            $scope.socket.emit('ret-rich-text', $scope._quill.getContents());
        });

        $scope.socket.on('res-rich-text', function (data) {
            $scope._quill.setContents(data);
        });

        $scope.socket.on('res-change-rich-text', function (data) {
            $scope._quill.updateContents(data);
        });

        $scope.socket.on('user-list', function (data) {
            $scope.userList = [];

            data.forEach(function (user) {
                $scope.userList.push({name: user, color: 'rgba(' + getRandom(100, 235) +
                            ',' + getRandom(100, 235) +
                            ',' + getRandom(100, 235) +
                            ',0.9)'});
            });

            $scope.cursorManager.cursors = {};
            $scope.userList.forEach(function (user) {
                if (user.name !== self.user)
                    $scope.cursorManager.setCursor(user.name, 0, user.name, user.color);
            });
            $scope.$apply();
        });

        this.socket.on('user-you', function (data) {
            $scope.user = data;
        });

        this.socket.on('res-change-selection', function (data) {
            if (data.range != null) {
                return $scope.cursorManager.moveCursor(data.user, data.range.end);
            }
        });
        
        this.socket.on('res-save-document', function (data) {
            $alertService.push(data);
        });        

        this.socket.on('disconnect', function () {
        });


        $scope._quill.on('text-change', function (delta, source) {
            if (source === 'user') {
                $scope.socket.emit('change-rich-text', delta);
                $scope.socket.emit('change-selection', $scope._quill.getSelection());
            }
        });

        $scope._quill.on('selection-change', function (range) {
            $scope.socket.emit('change-selection', range);
        });
    };
    
    $scope.save = function () {
        $scope.socket.emit('save-document');
    };

    $scope.loadDir = function (path) {
        $contentLoader.getUrl("/file/" + path);
    }

    $scope.$on('$destroy', function () {
        if ($scope.socket) {
            $scope.socket.disconnect();
        }
    });

});






