var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    owner: String,
    users: [],
    published: Boolean,
    path: String,
    type: String
});



module.exports = mongoose.model('File', userSchema);