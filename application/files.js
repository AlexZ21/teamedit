var fs = require("fs");
var mime = require('mime');
var git = require('./git');
var config = require('../config');
var File = require('./models/file');

module.exports = {
    createFile: function (user, filePath, callback) {
        fs.writeFile(filePath, "", function (err) {
            git(config.users_dir + user._id).exec('add', ['"' + filePath + '"'], function (gitErr, gitMsg) {
                console.log(gitErr, gitMsg);
                git(config.users_dir + user._id).exec('commit', ['--author="' + user.local.name + ' <' + user.local.email + '>"', '-m', '"File created"'],
                        function (gitErr, gitMsg) {

                            var file = new File();
                            file.owner = user.local.name;
                            file.users = [];
                            file.path = filePath;
                            file.published = true;
                            file.type = 'file';

                            file.save(function (err) {
                                if (err)
                                    throw err;

                                if (callback && typeof (callback) === "function") {
                                    callback(err);
                                }

                            });


                        });
            });
        });
    },
    createDir: function (user, filePath, callback) {
        git(config.users_dir + user._id).exec('add', ['"' + filePath + '"'], function (gitErr, gitMsg) {
            console.log(gitErr, gitMsg);
            git(config.users_dir + user._id).exec('commit', ['--author="' + user.local.name + ' <' + user.local.email + '>"', '-m', '"File created"'],
                    function (gitErr, gitMsg) {

                        fs.mkdir(filePath, function (err) {
                            var file = new File();
                            file.owner = user.local.name;
                            file.users = [];
                            file.path = filePath;
                            file.published = true;
                            file.type = 'dir';

                            file.save(function (err) {
                                if (err)
                                    throw err;

                                if (callback && typeof (callback) === "function") {
                                    callback(err);
                                }

                            });
                        });
                    });
        });
    },
    writeFile: function (user, filePath, data, callback) {
        fs.writeFile(filePath, data, function (err) {
            git(config.users_dir + user._id).exec('add', ['"' + filePath + '"'], function (gitErr, gitMsg) {
                console.log(gitErr, gitMsg);
                git(config.users_dir + user._id).exec('commit', ['--author="' + user.local.name + ' <' + user.local.email + '>"', '-m', '"File edited"'],
                        function (gitErr, gitMsg) {
                            if (callback && typeof (callback) === "function") {
                                callback(err);
                            }

                        });
            });
        });
    },
    removeFile: function (user, filePath, callback) {
        fs.unlink(filePath, function (err) {
            git(config.users_dir + user._id).exec('rm', ['"' + filePath + '"'], function (gitErr, gitMsg) {
                console.log(gitErr, gitMsg);
                git(config.users_dir + user._id).exec('commit', ['--author="' + user.local.name + ' <' + user.local.email + '>"', '-m', '"File removed"'],
                        function (gitErr, gitMsg) {

                            File.remove({path: filePath}, function (err) {
                                if (err)
                                    throw err;

                                if (callback && typeof (callback) === "function") {
                                    callback(err);
                                }
                            });

                        });
            });
        });
    },
    removeDir: function (user, filePath, callback) {
        fs.readdir(filePath, function (err, files) {
            if (err) {
                callback(err);
                return;
            }

            var wait = files.length;
            var count = 0;
            var dirDone = function (err) {
                count++;
                if (count >= wait || err) {
                    fs.rmdir(filePath, function (err) {
                        git(config.users_dir + user._id).exec('rm -r', ['"' + filePath + '"'], function (gitErr, gitMsg) {
                            console.log(gitErr, gitMsg);
                            git(config.users_dir + user._id).exec('commit', ['--author="' + user.local.name + ' <' + user.local.email + '>"', '-m', '"Folder removed"'],
                                    function (gitErr, gitMsg) {
                                        File.remove({path: filePath}, function (err) {
                                            if (err)
                                                throw err;

                                            if (callback && typeof (callback) === "function") {
                                                callback(err);
                                            }
                                        });
                                    });
                        });
                    });
                }
            };

            if (!wait) {
                dirDone();
                return;
            }

            filePath = filePath.replace(/\/+$/, "");
            files.forEach(function (file) {
                var curPath = filePath + "/" + file;
                fs.lstat(curPath, function (err, stats) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (stats.isDirectory()) {
                        module.exports.removeDir(curPath, dirDone);
                    } else {
                        fs.unlink(curPath, dirDone);
                    }
                });
            });
        });

    },
    getFileList: function (userDir, dirPath, callback) {

        fs.readdir(userDir + dirPath, function (readdirError, files) {

            if (readdirError) {
                if (callback && typeof (callback) === "function") {
                    callback();
                }
            } else {

                var f = {};

                files.forEach(function (file) {
                    fileStats = fs.statSync(userDir + dirPath + '/' + file);
                    f[file] = {};
                    f[file].path = dirPath + '/' + file;
                    f[file].name = file;

                    if (fileStats.isDirectory()) {
                        f[file].type = 'dir';
                    } else if (fileStats.isFile()) {
                        f[file].type = 'file';
                    }


                });

                if (callback && typeof (callback) === "function") {
                    callback(f);
                }
            }

        });

    },
    getFile: function (userDir, filePath, callback) {

        fs.readFile(userDir + filePath, function (err, data) {
            if (err) {
                throw err;
            }

            if (callback && typeof (callback) === "function") {
                callback(data);
            }

        });

    },
    getFileFP: function (filePath, callback) {

        fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
            if (err) {
                throw err;
            }

            if (callback && typeof (callback) === "function") {
                callback(data);
            }

        });

    },
    fileType: function (filePath, callback) {

        fs.stat(filePath, function (err, stat) {

            if (callback && typeof (callback) === "function") {

                if (err) {
                    throw err;
                }

                if (stat.isFile()) {
                    callback('file');
                } else if (stat.isDirectory()) {
                    callback('dir');
                }

            }

        });

    },
    getFileMime: function (filePath) {
        return mime.lookup(filePath);
    },
    exist: function (filePath, callback) {
        fs.stat(filePath, function (err, stat) {

            if (callback && typeof (callback) === "function") {

                if (err) {
                    callback(err);
                } else {
                    callback();
                }

            }

        });
    },
    rename: function (user, filePath, newFilePath, callback) {
        fs.rename(filePath, newFilePath, function (err) {
            git(config.users_dir + user._id).exec('add', ['"' + filePath + '"'], function (gitErr, gitMsg) {

                git(config.users_dir + user._id).exec('commit', ['--author="' + user.local.name + ' <' + user.local.email + '>"', '-m', '"Rename file"'],
                        function (gitErr, gitMsg) {


                            if (callback && typeof (callback) === "function") {
                                callback(err);
                            }

                        });
            });
        });
    },
    getFileHistoryList: function (user, filePath, callback) {

        git(config.users_dir + user._id).exec('log', ['"' + filePath + '"'],
                function (gitErr, gitMsg) {

                    var commitsStrings = gitMsg.split('commit');
                    var commits = [];

                    commitsStrings.forEach(function (commit) {
                        if (commit) {

                            var commitObject = {};

                            var commitRE = /^commit\s+(\S+)/i.exec(commit);
                            commitObject.hash = commit.split(/\s+/)[1];

                            var authorRE = /Author:\s+(.+?)\s+<(.+)>/.exec(commit);

                            commitObject.author = {
                                name: authorRE[1],
                                email: authorRE[2],
                            };

                            var dateRE = /Date:\s+(.+)\s+(\S.+)/.exec(commit);

                            commitObject.date = new Date(dateRE[1]);
                            commitObject.message = dateRE[2];


                            commits.push(commitObject);
                        }
                    });

                    if (callback && typeof (callback) === "function") {
                        callback(gitErr, commits);
                    }
                });

    },
    getFileHistory: function (user, commit, callback) {
        git(config.users_dir + user._id).exec('show', [commit],
                function (gitErr, gitMsg) {


                    var commitObject = {};

                    var commitRE = /^commit\s+(\S+)/i.exec(gitMsg);
                    commitObject.hash = gitMsg.split(/\s+/)[1];

                    var authorRE = /Author:\s+(.+?)\s+<(.+)>/.exec(gitMsg);
                    commitObject.author = {
                        name: authorRE[1],
                        email: authorRE[2],
                    };

                    var dateRE = /Date:\s+(.+)\s+(\S.+)/.exec(gitMsg);

                    commitObject.date = new Date(dateRE[1]);
                    commitObject.message = dateRE[2];

                    var neFileRE = /(new file mode)\s(.+)/.exec(gitMsg);
                    if (neFileRE) {
                        commitObject.newFile = neFileRE[2];
                    }

                    var changesRE = /@@\s+(.+)\s+@@\s?\-((.+)\s?(.+))\s?\+((.+)\s?(.+))/.exec(gitMsg);
                    if (changesRE) {
                        commitObject.changes = {
                            count: changesRE[1],
                            add: changesRE[3],
                            remove: changesRE[6]
                        };
                    }

                    console.log(gitErr, gitMsg);
                    if (callback && typeof (callback) === "function") {
                        callback(gitErr, commitObject);
                    }
                });


    },
    fileHistoryRevert: function (user, commit, filePath, callback) {
        git(config.users_dir + user._id).exec('checkout', [commit, '"' + filePath + '"'], function (gitErr, gitMsg) {
            console.log(gitErr, gitMsg);
            git(config.users_dir + user._id).exec('commit', ['--author="' + user.local.name + ' <' + user.local.email + '>"', '-m', '"Revert file state"'],
                    function (gitErr, gitMsg) {
                        console.log(gitErr, gitMsg);
                        if (callback && typeof (callback) === "function") {
                            callback(gitErr, gitMsg);
                        }
                    });
        });
    }

}