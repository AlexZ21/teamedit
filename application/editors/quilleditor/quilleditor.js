var files = require('../../files');
var Delta = require('rich-text').Delta;
var renderer = require('quilljs-renderer');
var Document = renderer.Document;
renderer.loadFormat('html');

module.exports.mimes = ['text/plain'];

module.exports.editor = QuillEditor;

function QuillEditor() {
    this.users = [];
    this.richText = new Delta();
}

QuillEditor.prototype.getClient = function () {
    return {
        files: [{type: 'css', path: '/public/css/editors/quilleditor/quill.snow.css'},
            {type: 'css', path: '/public/css/editors/quilleditor/quilleditor.css'},
            {type: 'js', path: '/public/js/editors/quilleditor/quill.min.js'},
            {type: 'js', path: '/public/js/editors/quilleditor/quilleditor.js'}],
        'ejs': 'editors/quilleditor/quilleditor'
    }
}

QuillEditor.prototype.userConnected = function (user, socket) {
    var qe = this;
    this.users.push(user.local.name);

    this.socket = socket;

    if (this.users.length === 1) {
        socket.emit('first-user', this.data);
    } else {
        socket.emit('res-rich-text', qe.richText);
    }

    socket.emit('user-list', this.users);
    socket.emit('user-you', user.local.name);
    socket.broadcast.emit('user-list', this.users);

    socket.on('ret-rich-text', function (richText) {
        var delta = new Delta(richText.ops);
        qe.richText.compose(delta);
    });

    socket.on('change-rich-text', function (richText) {
        var delta = new Delta(richText.ops);
        qe.richText.compose(delta);
        socket.broadcast.emit('res-change-rich-text', delta);
    });

    socket.on('change-selection', function (range) {
        socket.broadcast.emit('res-change-selection', {user: user.local.name, range: range});
    });

    socket.on('save-document', function () {
        var doc = new Document(qe.richText);
        files.writeFile(user, qe.filePath, doc.convertTo('html'), function (err) {
            if (err) {
                socket.emit('res-save-document', { type: 'danger', msg: "Can't save file" });
            } else {
                socket.emit('res-save-document', { type: 'success', msg: "File saved" });
            }
        });
    });

    if (this.users.length === 0) {
        socket.emit('message', this.data);
    }
}

QuillEditor.prototype.userDisconected = function (user) {
    var index = this.users.indexOf(user.local.name);
    this.users.splice(index, 1);

    if (this.users.length > 0) {
        this.socket.broadcast.emit('user-list', this.users);
    }
}

QuillEditor.prototype.message = function (user, message) {
    console.log(message);
}

QuillEditor.prototype.openFile = function (filePath, callback) {
    var editor = this;
    this.filePath = filePath;

    files.getFileFP(filePath, function (data) {

        editor.data = data;

        if (callback && typeof (callback) === "function") {
            callback();
        }

    });
}

QuillEditor.prototype.saveFile = function (callback) {

    if (callback && typeof (callback) === "function") {
        callback();
    }

}