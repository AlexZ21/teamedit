var LocalStrategy = require('passport-local').Strategy;
var User = require('./models/user');
var File = require('./models/file');
var fs = require('fs');
var git = require("./git");

module.exports = function (app) {



    app.passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    app.passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    app.passport.use('local-signup', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    function (req, email, password, done) {

        process.nextTick(function () {

            User.findOne({'local.email': email}, function (err, user) {

                if (err)
                    return done(err);

                if (user) {
                    return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                } else {

                    var newUser = new User();

                    newUser.local.email = email;
                    newUser.local.password = newUser.generateHash(password);
                    newUser.local.name = req.body.name;

                    newUser.save(function (err) {
                        if (err)
                            throw err;
                        fs.mkdir(app.config.users_dir + newUser._id, function () {
                            git(app.config.users_dir + newUser._id).exec('init', function (gitErr, gitMsg) {
                                git(app.config.users_dir + newUser._id).exec('config', ['user.name', '"' + newUser.local.name + '"'], function (gitErr, gitMsg) {
                                    git(app.config.users_dir + newUser._id).exec('config', ['user.email', '"' + newUser.local.email + '"'], function (gitErr, gitMsg) {

                                        var file = new File();
                                        file.owner = newUser.local.name;
                                        file.users = [];
                                        file.path = app.config.users_dir + newUser._id;
                                        file.published = true;
                                        file.type = 'dir';

                                        file.save(function (err) {
                                            if (err)
                                                throw err;

                                            return done(null, newUser);

                                        });

                                    });
                                });
                            });

                        });
                    });
                }

            });

        });

    }));

    app.passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    function (req, email, password, done) {

        User.findOne({'local.email': email}, function (err, user) {

            if (err)
                return done(err);

            if (!user)
                return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

            return done(null, user);
        });

    }));

};