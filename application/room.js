var passportSocketIo = require("passport.socketio");
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var cookieParser = require('cookie-parser');

module.exports = Room;

function Room(app, roomIndex, filePath) {
    this.app = app;
    this.roomIndex = roomIndex;
    this.file = filePath;

    this.sockets = [];

    this.io = require('socket.io')();

    this.io.use(passportSocketIo.authorize({
        cookieParser: cookieParser,
        key: 'tmsid',
        secret: app.config.secret,
        store: app.sessionStore,
        success: onAuthorizeSuccess, // *optional* callback on success - read more below
        fail: onAuthorizeFail, // *optional* callback on fail/error - read more below
    }));

    this.io.set('origins', 'http://teamedit:3004');

}

Room.prototype.setEditor = function (editor) {
    var app = this.app;
    var room = this;

    if (!this.editor) {
        this.editor = editor;
        this.editor.openFile(this.file, function () {

            room.io.on('connection', function (socket) {

                room.sockets.push(socket);
                editor.userConnected(socket.request.user, socket);

                socket.on('disconnect', function () {
                    editor.userDisconected(socket.request.user);

                    var index = room.sockets.indexOf(socket);
                    room.sockets.splice(index, 1);

                    if (room.sockets.length === 0) {
                        room.io.close();
                        delete app.rooms[room.roomIndex];
                    }

                });


            });

        });
    }
}

Room.prototype.getEditor = function () {
    return this.editor;
}

Room.prototype.open = function () {
    this.port = this.app.getPort();
    this.io.listen(this.port);
}

function onAuthorizeSuccess(data, accept) {
    console.log('successful connection to socket.io');
    accept();
}

function onAuthorizeFail(data, message, error, accept) {
    console.log('failed connection to socket.io:', message);
    if (error)
        accept(new Error(message));
}
