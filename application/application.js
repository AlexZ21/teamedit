var express = require('express');
var config = require('../config');
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var cors = require('cors');

module.exports = Application;



function Application() {

    var expressInstance = express();

    expressInstance.config = config;
    expressInstance.controllers = {};
    expressInstance.rooms = {};
    expressInstance.roomsLinks = {};
    expressInstance.editors = {};

    expressInstance.currentPort = 5000;

    expressInstance.use(config.public_url, express.static(config.public_dir));
    expressInstance.set('views', config.view_dir);
    expressInstance.set('view engine', 'ejs');

    expressInstance.mongoose = mongoose;
    expressInstance.mongoose.connect(config.mongo_url);

    expressInstance.passport = passport;
    require('./passport')(expressInstance);

    expressInstance.use(morgan('dev'));

    expressInstance.use(bodyParser.urlencoded({extended: false}));
    expressInstance.use(bodyParser.json());

    expressInstance.use(cookieParser());
    
    expressInstance.use(cors());

    //db: mongoose.connection.db mongooseConnection: expressInstance.mongoose.connection
    expressInstance.sessionStore = new MongoStore({mongooseConnection: expressInstance.mongoose.connection});
    expressInstance.use(session({secret: config.secret,
        name: 'tmsid',
        store: expressInstance.sessionStore}));

    expressInstance.use(passport.initialize());
    expressInstance.use(passport.session());
    expressInstance.use(flash());



    // Controllers
    expressInstance.loadControllers = function () {

        config.controllers.forEach(function (controllerName) {
            expressInstance.controllers[controllerName] =
                    require(config.controllers_dir + controllerName)(expressInstance);
        });

    }



    // Load controllers
    expressInstance.loadControllers();



    //Editors
    expressInstance.loadEditors = function () {

        config.editors.forEach(function (editorPath) {
            var editor = require('./editors/' + editorPath);

            editor.mimes.forEach(function (mime) {
                expressInstance.editors[mime] = editor.editor;
            });

        });

    }



    // Load controllers
    expressInstance.loadEditors();



    // Get new port
    expressInstance.getPort = function () {
        return expressInstance.currentPort++;
    }

    return expressInstance;

}


