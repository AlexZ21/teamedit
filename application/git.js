var Git = require('git-wrapper');

module.exports = git;

function git(path) {

    return new Git({'git-dir': path + '/.git', 'work-tree': path});

}

