module.exports = {
    'base_dir': '/home/alex/Workspace/node.js/sites/teamedit/',
    'controllers_dir': '/home/alex/Workspace/node.js/sites/teamedit/controllers/',
    'view_dir': '/home/alex/Workspace/node.js/sites/teamedit/view/',
    'public_dir': '/home/alex/Workspace/node.js/sites/teamedit/public/',
    'public_url': '/public',
    'users_dir': '/home/alex/Workspace/node.js/sites/teamedit/users/',
    
    
    controllers: [
        'index',
        'users',
        'home',
        'file',
        'room'
    ],
    
    editors: [
        'quilleditor/quilleditor'
    ],
    
    
    mongo_url: 'mongodb://localhost:27017/teamedit',


    secret: 'iloveteamedit'

};